configfile: "config.yaml"

sample = config["samples"]

rule all:
    input:
        expand("Counts/{sample}.count", sample = config["samples"])

rule getData:
        output:
                "RawData/{sample}.fastq.gz", temp("{sample}.sra")
        wildcard_constraints:
                sample="SRR\d+"
        benchmark:
                "benchmarks/getData/{sample}.txt"
        message:
                "I'm currently busy downloading {input}.fastq.gz"
        log:
                "logs/getData/{sample}.log"
        run:
                # String manipulation to get the url
                command = "wget ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/{0}/{1}/{1}.sra".format(wildcards.sample[0:6], wildcards.sample)
                command2 = "fastq-dump --gzip -O RawData/ " + wildcards.sample + ".sra"
                import subprocess
                process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
                output, error = process.communicate()
                process = subprocess.Popen(command2.split(), stdout=subprocess.PIPE)
                output, error = process.communicate()

rule unzip:
    input:
        "RawData/{sample}.fastq.gz"
    output:
        "RawData/{sample}.fastq"
    benchmark:
        "benchmarks/unzip/{sample}.txt"
    message:
        "Meanwhile unzipping {input}"
    shell:
        "gunzip {input}"

rule fastqc:
    input:
        "RawData/{sample}.fastq"
    output:
        "RawData/{sample}_fastqc.html"
    benchmark:
        "benchmarks/fastqc/{sample}.txt"
    message:
        "I'm performing fastqc analysis right now"
    log:
        "logs/fastqc/{sample}.log"
    shell:
        "fastqc {output} {input}"
        
rule annotate_refGenome:
    input:starref="STARgenome"
    output:
        "STARgenome"
    message:
        "Now I'm annotating the {config[genome]} reference genome"
    shell:
        "{{STAR}} --limitGenomeGenerateRAM 150000000000 --runMode genomeGenerate --genomeDir {input.starref} --runThreadN 20 --genomeFastaFiles {config[genome]} --sjdbGTFfile {config[gtf]}"


rule star_map_count:
    input:
        sample="RawData/{sample}.fastq.gz", starref="STARgenome"
    output:
        "Counts/{sample}.count"
    benchmark:
        "benchmarks/star_map_count/{sample}.txt"
    message:
        "Mapping and generating count data later on"
    log:
        "logs/star_map_count/{sample}.log"
    shell:
        "STAR --genomeDir {input.starref} --outFileNamePrefix {input.sample} --readFilesIn {input} --runThreadN 24 --readFilesCommand gunzip -c --quantMode GeneCounts -outStd SAM > {output}"
