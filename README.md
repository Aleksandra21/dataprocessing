# Generate Counts
This pipeline produces count data from SRR accession numbers.   
It does this by first downloading the data from NCBI's SRA database using the given SRR accession numbers from the config file.    
And then later on annotating the reference genome, mapping and finally generating count data.

### Prerequirements
* Python 3 package
* Python3 environment
* fastq-dump 2.8.1 
  (included in the SRA Toolkit: https://www.ncbi.nlm.nih.gov/sra/docs/toolkitsoft/)
* psutil package to use benchmarks
* Fastqc tool (optional): http://www.bioinformatics.babraham.ac.uk/projects/fastqc/
* GTF file of chosen organism:
  ftp://ftp.ensembl.org/pub/release-95/gtf/
* Reference genome of same organism:
  ftp://ftp.ensembl.org/pub/release-95/fasta/
* STAR (Spliced Transcripts Alignment to a Reference):
  https://github.com/alexdobin/STAR/archive/2.7.1a.tar.gz
  
  
### Configuration file:
genome:
Location of your genome file -> directory + name

gtf:
The gtf file location -> directory + name 

samples:
A list of SRR accession numbers to generate count data 



### How to run the pipeline
* First you need to be in the same directory as the pipeline
  if that is not the case then go to that directory
* Second check in all the needed files are in that directory and in the config file
* Now you can type the following in your terminal to run the pipeline:
	
	```
	snakemake  --snakefile [filename] -- cores [number of cores]
	```

## Author
Aleksandra Kapielska

## License
This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details

